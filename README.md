# **First Person Shooter**


Dejo [aquí](https://www.youtube.com/watch?v=aGeX08afeo8) el enlace al vídeo de demostración.


## **Cómo jugar**

<div style="text-align: justify"> 

El juego consta de tres escenas principales por las que navegar durante la partida: menú principal, nivel 1 y pantalla final. En caso de crear un nuevo nivel, sólo hay que añadirlo a la lista y cargarlo una vez se termina el nivel superado. 

- Menú principal: Consta de dos botones para iniciar la partida y para salir de la aplicación. 

- Nivel 1: Correspondiente al primer nivel de juego. Aquí podemos controlar al jugador mediante las flechas del teclado o las teclas **A, S, D y W**. Se permite saltar pulsando la barra espaciadora, recoger objetos pulsando la tecla **Q**, recargar el arma pulsando la tecla **R** y cambiar de una a otra con los números, siendo **1** el arma principal, **2** la secundaria y **3** la terciaria. Movemos la cámara con el ratón para apuntar y apretamos el botón derecho para disparar una vez o mantenemos pulsado para disparar continuamente. 

- Game Over: Se muestra una vez superado el juego y dispone de un botón para volver al menú principal y un botón para cerrar la aplicación.

<p>
<img style="align: center" width="190px"src="images/main_menu.png") />
<img style="align: center" width="190px"src="images/level_1.png") />
<img style="align: center" width="190px"src="images/game_over.png") />
</p>

</div>

## **Estructura de juego**

<div style="text-align: justify"> 

Con tal de hacer el juego modular y fácilmente ampliable, se ha dedicado mucho tiempo a la implementación de scripts que funcionarían perfectamente en cualquier juego similar. Dada la complejidad del proyecto, vamos a separar por funcionalidades la implementación:

> ### **Player scripts**
> Incorpora las funcionalidades correspondientes al sistema de estadísticas del jugador, la muestra por pantalla del HUD, el inventario y la gestión de sus objetos (así como el recogerlos), el cambio de armas y la gestión de los disparos.
>
> **1. Sistema de estadísticas:** Tanto para el jugador como para los enemigos, se ha creado una clase genérica llamada **Character Stats** de la cual heredan **Player Stats** y **Enemy Stats**. Dicha clase contiene los siguientes métodos:
>
>   1. `public virtual void CheckHealth(){}:` Método virtual que se sobrescribe en las clases que heredan de esta. Encargado de comprobar la vida del sujeto y evitar que sobrepase los valores mínimos y máximos. Llama al método **Die()** si la salud llega al mínimo.
> 
>   2. `public virtual void CheckShield(){}:` Dispone de la misma funcionalidad que **Check Health** en el caso de que se disponga de escudo.
>
>   3. `public virtual void Die(){}:` Método virtual que pone el booleano *isDead* a true cuando la vida llega a 0.
>
>   4. `public void SetHealthTo(float healthToSet){}:` Método útil para fijar la salud del sujeto al valor que se pasa por parámetro. Llama al método **Check Health()** para evitar sobrepasar los límites y comprobar si el sujeto ha muerto.
>
>   5. `public void SetShieldTo(float shieldToSet)` Método útil para fijar el escudo del sujeto al valor que se pasa por parámetro. Llama al método **Check Shield()** para evitar sobrepasar los límites.
>
>   6. `public virtual void TakeDamage(int dmg){}:` Método virtual que se sobrescribe en las clases que heredan de esta. Comprueba si el sujeto dispone de escudo para calcular la cantidad de daño a recibir. En el caso de disponer de escudo, éste se llevará el 90% del daño y la salud un 10%. En caso contrario, la salud se llevará el 100%. Llama a los métodos **SetHealthTo()** y **SetShieldTo()** para establecer los valores correspondientes. 
>
>   7. `public void Heal(int heal){}:` Método encargado de sumar a la vida del sujeto la cantidad de salud deseada. Llama a **SetHealthTo()** para establecer dicho valor.
>
>   8. `public void AddShield(int shieldToAdd){}:` Método encargado de sumar al escudo del sujeto la cantidad de escudo deseada. Llama a **SetShieldTo()** para establecer dicho valor.
>
>   9. `public virtual void InitVariables(){}:` Método virtual que inicializa los valores de vida y escudo, así como el booleano isDead. Éste se sobrescribe en otras clases con los valores deseados para cada sujeto. 
>
> En el caso del Player, la clase que hereda de **Character Stats** se llama **Player Stats**. Dicha clase sobrescribe los siguientes métodos:
>
>   1. `public override void Die(){}:` Misma funcionalidad que el parent con el añadido de la llamada al método **Respawn()**, encargado de hacernos reaparecer en los checkpoints estipulados.
> 
>   2. `public override void CheckHealth(){}:` Añade la actualización de la vida en el HUD mostrado por pantalla.
>
>   3. `public override void CheckShield(){}:` Añade la actualización del escudo en el HUD mostrado por pantalla.
>
>   4. `public override void TakeDamage(int dmg){}:` Añade sonido cuando recibimos daño.
>
> **2. HUD:** Consta de cuatro métodos encargados de actualizar la UI del jugador: **UpdateHealth()**, **UpdateShield()**, **UpdateWeaponUI()** y **UpdateAmmoUI()**.
> 
> ```
>   public class PlayerHUD : MonoBehaviour
>    {
>        [SerializeField] private ProgressBar healthBar;
>        [SerializeField] private ProgressBar shieldBar;
>        [SerializeField] private InventoryUI inventoryUI;
>
>    public void UpdateHealth(float currentHealth, float maxHealth)
>    {
>        healthBar.SetValues(currentHealth, maxHealth);
>    }
>
>    public void UpdateShield(float currentShield, float maxShield)
>    {
>        shieldBar.SetValues(currentShield, maxShield);
>    }
>
>    public void UpdateWeaponUI(Weapon newWeapon)
>    {
>        inventoryUI.UpdateInfo(newWeapon.icon, newWeapon.magazineSize, newWeapon.storedAmmo);
>    }
>
>    public void UpdateAmmoUI(int currentAmmo, int storedAmmo)
>    {
>        inventoryUI.UpdateAmmoUI(currentAmmo, storedAmmo);
>    }
>}
> ```
>
> **3. Inventory:** Clase encargada de gestionar el inventario del jugador. Básicamente añade objetos, los elimina y nos permite acceder a ellos desde otras clases mediante el método **GetItem()**. Ésta clase dispone del método **AddItem()**, que comprueba el tipo de objeto que se ha recogido y ejecuta un código u otro en función de ello.
>
> Como sólo podemos almacenar llaves y armas, si el objeto que se recibe por parámetro es una llave se llama a **AddKey()** y se le pasa por parámetro el objeto de tipo **Key**. En caso de ser un arma, se llama a **AddWeapon()** y se le pasa por parámetro el objeto de tipo **Weapon**.
>
> ```
>    public void AddItem(Item newItem)
>    {
>        if(newItem is Key)
>        {
>            AddKey(newItem as Key);
>        }
>
>        if(newItem is Weapon)
>        {
>            AddWeapon(newItem as Weapon);
>        }
>        newItemIndex++;
>    }
>
>    public void AddKey(Key newKey)
>    {
>        hasKey = true;
>        if (newItemIndex < maxNumOfItems)
>        {
>            items[newItemIndex] = newKey;
>        }
>        else
>            Debug.Log("Max number of items stored reached");
>    }
>
>    public void AddWeapon(Weapon newWeapon)
>    {
>        int newItemIndex = (int)newWeapon.weaponStyle;
>
>        if (weapons != null)
>        {
>            if (weapons[newItemIndex] != null)
>            {
>                RemoveItem(newItemIndex);
>            }
>            weapons[newItemIndex] = newWeapon;
>        }
>
>        shooting.InitAmmo((int)newWeapon.weaponStyle, newWeapon);
>    }
>```
>
> **4. Equipment Manager:** Lleva el registro del arma que tenemos equipada y nos permite cambiar de una a otra pulsando la tecla adecuada. Además, actualiza el HUD con la información del arma que llevamos al momento de cambiar (icono, munición y almacenaje). 
> 
> Para ello, en el método **Update()** se comprueba si se pulsa la tecla 1 o la tecla 2 para equipar el arma que corresponde; comprobando primero que el arma no esté ya equipada. En caso de no estarlo, se desequipa el arma anterior y se equipa la seleccionada llamando a **EquipWeapon()** y pasando por parámetro el objeto del inventario. Dicha función instancia el arma en cuestión en las manos del jugador y actualiza los datos del HUD. El método **UnequipWeapon()**, sencillamente destruye la instancia anterior. 
>
> ```
>    private void Update()
>   {
>        if (Input.GetKeyDown(KeyCode.Alpha1) && currentlyEquipedWeapon != 0)
>        {
>            UnequipWeapon();
>            EquipWeapon(inventory.GetItem(0));
>        }
>        if (Input.GetKeyDown(KeyCode.Alpha2) && currentlyEquipedWeapon != 1)
>        {
>            if (inventory.GetItem(1) != null)
>            {
>                UnequipWeapon();
>                EquipWeapon(inventory.GetItem(1));
>            }
>        }
>    }
> ```
> Es importante tener en cuenta que sólo se nos permite llevar 2 armas y estas podrán ser de tipo *Primary* o *Secondary* (podrían ser otras). Esto es fácilmente ampliable y se verá más en detalle en el apartado de Items. Nuestro jugador llevará equipada una pistola por defecto nada más empezar la partida. 
>
> **5. Player Pickup:** Permite recoger objetos mediante la tecla **Q**. Se hace uso de **Physics.Raycast()** para detectar con qué tipo de objeto se ha colisionado y en función de eso se añade al inventario de una forma u otra. Además, destruye el objeto de la escena y nos muestra por pantalla un texto con el objeto recogido. 
> 
> ````
>    private void Update()
>    {
>        if (Input.GetKeyDown(KeyCode.Q))
>        {
>            AudioSource audioSource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
>            audioSource.clip = pickupClip;
>            audioSource.Play();
>
>            RaycastHit hit;
>            Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
>
>
>            if (Physics.Raycast(ray, out hit, pickupRange, pickupLayer))
>            {
>                Weapon newWeapon = hit.transform.GetComponent<ItemObj>().item as Weapon;
>                inventory.AddItem(newWeapon);
>                Destroy(hit.transform.gameObject);
>                pickupPanel.SetActive(true);
>                pickupPanelText.text = "WEAPON ADDED TO YOUR INVENTORY";
>                StartCoroutine(HidePanel());
>            }
>
>            if (Physics.Raycast(ray, out hit, pickupRange, keyLayer))
>            {
>                Key newItem = hit.transform.GetComponent<ItemObj>().item as Key;
>                inventory.AddItem(newItem);
>                Destroy(hit.transform.gameObject);
>                pickupPanel.SetActive(true);
>                pickupPanelText.text = "KEY ADDED TO YOUR INVENTORY";
>                StartCoroutine(HidePanel());
>            }
>        }
>    }
> ````
>
> **6. Shooting:** La clase shooting es bastante compleja debido a la cantidad de cosas a gestionar. Dicha clase incluye la funcionalidad de disparo y la de recarga (probablemente podrían estar separadas en dos clases diferentes y convendría un pequeño refactor). Para disparar, podemos mantener pulsado el botón derecho del ratón y será el método **Shoot()** el encargado de gestionar el fire rate en función del arma equipada. Por otro lado, para recargar basta con pulsar la tecla **R** y se llama al método **Reload()**. Dada la complejidad de la clase, iremos por partes detallando cada uno de los métodos:
>
>   1. `private void Shoot(){}:` Comprueba el arma que llevamos equipada accediendo al inventario mediante el método **GetItem()**. Hecho esto, usamos el arma como parámetro de entrada al método **CheckCanShoot()**, que comprobará el slot (0 si es Primary y 1 si es Secondary) para verificar que tenemos balas en el cargador. En caso que nos sea posible disparar, comprobamos el fire rate del arma para lanzar un rayo mediante **RaycastShoot()** y gastaremos munición llamando a **UseAmmo()**. De lo contrario, mostará un mensaje en la consola avisando que el cargador está vacío. 
>
> ````
>private void Shoot()
>    {
>        Weapon currentWeapon = inventory.GetItem(equipmentManager.currentlyEquipedWeapon);
>
>        CheckCanShoot(equipmentManager.currentlyEquipedWeapon);
>        InitAudio(currentWeapon);
>
>        if (canShoot && canReload)
>        {
>            if (Time.time > lastFireTime + currentWeapon.fireRate)
>            {
>                lastFireTime = Time.time;
>
>                RaycastShoot(currentWeapon);
>                UseAmmo((int)currentWeapon.weaponStyle, 1, 0);
>                gunAudio.clip = currentWeapon.fireFX;
>                gunAudio.Play();
>            }
>        }
>        else
>            Debug.Log("Empty Magazine");
>    }
> ````
> 
>   2. `private void RaycastShoot(Weapon currentWeapon){}:` Método que sirve para detectar si nuestra bala ha colisionado con otro objeto. Se lanza un rayo desde el centro de la pantalla en la dirección en la que estamos mirando y cuyo rango está determinado por el rango del arma equipada. En caso de que se produzca una colisión con un enemigo, recogemos sus estadísticas para aplicar el daño del arma equipada y poner a true un bool que alerta al oponente. Como extra, se instancian Decals cuando chocamos contra un objeto y se elimina el primero de ellos cuando llegamos a un máximo de 10.
>
> ````
>    private void RaycastShoot(Weapon currentWeapon)
>    {
>        Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
>        RaycastHit hit;
>
>        float currentWeaponRange = currentWeapon.range;
>
>        if (Physics.Raycast(ray, out hit, currentWeaponRange))
>        {
>            if (hit.collider.gameObject.tag == "Enemy")
>            {
>                CharacterStats enemyStats = hit.collider.gameObject.GetComponentInParent<CharacterStats>();
>                EnemyAI enemyAI = hit.collider.gameObject.GetComponentInParent<EnemyAI>();
>                enemyAI.isHit = true;
>                DealDamage(enemyStats, currentWeapon.damage);
>            }
>
>            if (hit.collider.gameObject.tag != "Enemy")
>            {
>                decalInstance = GameObject.Instantiate(decalPrefab, hit.point + hit.normal * 0.01f, Quaternion.FromToRotation(Vector3.forward, -hit.normal));
>                decalInstance.transform.SetParent(hit.transform);
>            }
>
>            if (decals.Count >= maxDecalsAllowed)
>            {
>                GameObject.Destroy(decals[0]);
>                decals.RemoveAt(0);
>            }
>
>            decals.Add(decalInstance);
>        }
>
>        Instantiate(currentWeapon.muzzleFlashParticles, equipmentManager.currentWeaponBarrel);
>    }
>```` 
>
>   3. `private void UseAmmo(int slot, int currentAmmoUsed, int currentStoredAmmoUsed){}:` Comprueba si disponemos de munición en el slot de arma equipado y, de ser así, resta la cantidad de munición recibida por parámetro al cargador y al almacenaje. 
>
>   4. `private void Reload(int slot){}:` Comprueba si podemos recargar en ese momento y el slot al cual queremos aplicar la recarga. Calcula la cantidad de munición a recargar restando la cantidad actual de munición al tamaño del almacenaje. Comprueba que el cargador no esté lleno y, si no lo está, añade munición al cargador mediante **AddAmmo()** y la resta del almacenaje mediante **UseAmmo()**. Por último, comprueba si tenemos suficiente munición como para recargar el arma. 
>
>   5. `public void AddAmmo(int slot, int currentAmmoAdded, int currentStoredAmmoAdded){}:` Añade la cantidad de munición especificada al slot pertinente. 
>
>   6. `public void InitAmmo(int slot, Weapon weapon){}:` Inicializa la munición en función del slot y los valores del arma. 
>
>   7. `public void DealDamage(CharacterStats statsToDamage, int damage){}:` Llama a la función **TakeDamage()** del enemigo para restarle vida.

> ### **Enemy scripts**
> 
> Este apartado incluye información sobre la máquina de estados de los enemigos y su sistema de estadísiticas. Para ello, se ha hecho uso de la interfaz **IEnemyState**, ademas de los scripts correspondientes a cada uno de los estados y el manager, y de la herencia de **CharacterStats** para la implementación de **EnemyStats**. 
>
> - **EnemyStats**: Hereda de **CharacterStats** y sobrescribe un par de sus métodos: **CheckHealth** y **Die**. En el caso del primero, sencillamente actualiza la UI. Para el segundo, tiene en cuenta una lista con el total de enmigos para ir restando a medida que mueren. Además, se encarga de de llamar a la función **DropLoot()** que forma parte de **LootableObj** y permitirá que suelten objetos con cierta probabilidad. Por último, destruye el objeto e instancia una explosión. También dispone de algunos métodos adicionales:
>
>   1. `public void DealDamage(CharacterStats statsToDamage){}:` Aplica daño a los stats del jugador.
>
>   2. `private void PlayDieSound(GameObject gameObject){}:` Lanza un clip de sonido de explosión tras morir.
>
>   La vida y el daño que realizan los enemigos se asigna mediante el inspector de Unity. Esto nos permite crear distintos enemigos fácilmente, cambiando sus parámetros y creando nuevos prefabs. 
>
> - **FSM:** Máquina de estados que determina el comportamiento de los enemigos durante el juego. Se conforma de los siguientes scripts:
>
>   1. `IEnemyState:` Interfaz que define las funciones que deben implementar las clases que hereden de ella.
>
>   2. `EnemyAI:` Encargado de gestionar el estado actual. Se crea una instancia de esta clase en cada uno de los estados para saber en todo momento en qué punto de la máquina de estados nos encontramos. 
>
>   3. `PatrolState:` Ofrece movimiento a los enemigos mediante una lista de waypoints. Además, dispone de métodos de trigger para la detección del jugador. Permite pasar a los estados **Alert** y **Attack** si detecta al jugador o recibe un disparo, respectivamente. Fija el spotlight al color verde.
>
>   3. `AlertState:` Al entrar en estado de alerta, en el **Update()** se hace rotar al enemigo simulando una búsqueda del jugador y se cambia la luz al amarillo. Si en el transcurso de la vuelta no se le ha detectado, volverá a patrullar. En caso de que se detecte al jugador mediante raycasting, pasará al estado **Attack**. Si esto ocurre, nos aseguramos que el bool **shouldFollow** esté a true para que los enemigos persigan al player si este está dentro del radio de acción. 
>
>   1. `AttackState:` Estado al que se entra si el enemigo recibe un disparo o detecta al jugador tras el estado de alerta. Cambia la luz al color rojo y llama a **LookAtTarget()** y a **FollowTarget**. De esta forma los enemigos mirarán siempre al jugador y le perseguirán hasta llegar a cierta distancia. Por otro lado, mientras el jugador esté dentro del trigger, el enemigo le atacará cada vez que supere el tiempo de ataque predeterminado.
>
> - **EnemyUI:** Para culminar, se añade una barra de vida justo encima de la cabeza del enemigo y se gestiona de la misma forma que la UI del jugador. 
>

> ### **UI**
> Esta parte del código es muy sencilla. Por un lado, se encarga de gestionar las barras de salud y escudo de enemigos y jugador mediante el script **ProgressBar**. Dicha clase contiene dos métodos principales **SetValues()** y **CalcFillAmount()**:
>
> ```
>    public void SetValues(float _baseValue, float _maxValue)
>    {
>        baseValue = _baseValue;
>        maxValue = _maxValue;
>
>        amount.text = baseValue.ToString();
>
>        CalcFillAmount();
>    }
>
>    public void CalcFillAmount()
>    {
>        float fillAmount = baseValue / maxValue;
>        fill.fillAmount = fillAmount;
>    }
> ```
> Estos métodos reciben los valores correspondientes a los textos y la cantidad de fill de las imagenes de la UI. 
>
> Por otro lado, existe la clase **InventoryUI** cuyo métodos son **UpdateInfo()** y **UpdateAmmoUI()**. Ambas se encargan de gestionar de un modo u otro la información de los iconos y la munición mostrada por pantalla.
>

> ### **GameIssues**
>Este apartado incluye varios aspectos del juego que se han ido comentando por encima en el resto de apartados. Además, también contiene scripts de gestión de la partida como el **GameManager**. Vayamos primero a los scripts sueltos y, a posteriori, vemos el resto por apartados:
>
> - **GameManager:** Se considera que se ha terminado el nivel (en este caso el juego) una vez eliminados todos los enemigos. Para ello, se crea un array de enemigos que va decreciendo conforme los vamos matando. Si la cuenta total de enemigos llega a 0, se llama a **EndGame()** y se carga la pantalla final (o el siguiente nivel en caso de haberlo).
>
>   La clase en cuestión también contiene el método **Respawn()**. Dicho método se encarga de desactivar el **CharacterController** y mover al jugador a los checkpoints correspondientes. Al parecer, el **CharacterController** bloquea los reposicionamientos del jugador, es por eso que se debe desactivar y reactivar una vez movido el jugador. Además, se resetean las estadísticas del jugador como la vida, el escudo y el bool isDead.
>
> - **CheckpointManager:** Script muy sencillo que cambia el checkpoint al que va a parar el jugador una vez muerto.
>
> - **Elevator:** Gestiona el movimiento del ascensor entre las dos plantas. Para ello, usa triggers para determinar si se debe mover de planta o no. En caso de estar en la planta 0 (origin), si detecta al jugador entrar se moverá del origen al destino y viceversa.  
>
> - **Doors**: Este apartado se encarga de gestionar las diferentes puertas que puedan existir en el juego. De esta forma, sólo se han implementado puertas que se abren automáticamente y puertas que necesitan llave. Aún así, sería fácilmente ampliable en caso de querer añadir más tipos. Básicamente, generamos los tipos de puertas heredando de la interfaz **IDoor**. Dicha interfaz obliga a las clases que heredan a implementar los métodos **OpenDoor()** y **CloseDoor()**. 
>
>   Así pues, de la misma forma que se actualiza la máquina de estados de los enemigos, se actualiza el tipo de puerta frente a la que nos encontramos.  Éstas son:
>
>   1. `TriggerDoor:` Se abre y se cierra sin ningún impedimento. Cuando el jugador entra en el trigger, se llama a **OpenDoor()** y cuando sale se llama a **CloseDoor()**.
>
>   1. `KeyDoor:` El método **OpenDoor()** depende de si existe una llave en el inventario para abrir o no la puerta. En caso afirmativo, la puerta se abrirá. De lo contrario, nos mostrará un panel que indica que necesitamos una llave para abrirla. 
>
>    ````
>    public void OpenDoor()
>    {
>        if (doorManager.inventory.hasKey)
>        {
>            doorManager.audioSource.Play();
>            doorManager.GetComponent<Animator>().Play("Door_Open");
>        }
>        else
>        {
>            doorManager.keyPanel.SetActive(true);
>        }
>    }
>   ````
>
> - **Menus:** Hay varios scripts que gestionan los menús de juego. La mayoría de ellos son sencillos, como el menú principal o la pantalla final. Sencillamente constan de varios listeners para cada botón que cargan la escena adecuada o cierran la aplicación. 
>
>   El único al que vale la pena prestarle un poco más de atención es el **PauseManager**. Para hacer que el menú de pausa funcione correctamente, mostrar el cursor (oculto durante el juego) y confinarlo para que no pueda salir de la pantalla. Además, hay que desactivar el **FirstPersonController** y el script **Shooting** para inhabilitar la cámara y la función de disparo. De esta forma, nos aseguramos que el juego queda congelado y no interfiere en el menú de pausa y sus interacciones.
>
>   ```
>    private void DisableControls()
>    {
>        GameObject player = GameObject.FindGameObjectWithTag("Player");
>        Cursor.lockState = CursorLockMode.Confined;
>        Cursor.visible = true;
>        player.GetComponent<FirstPersonController>().enabled = false;
>        player.GetComponent<Shooting>().enabled = false;
>    }
>   ```
>
> - **Items:** Para la creción de objetos almacenables en el inventario se ha hecho uso de Scriptable Objects. Básicamente, tenemos una clase **Item** que hereda de **ScriptableObject** y que contiene un string con el nombre del item y un Sprite con el icono. Partiendo de aquí, se crean dos clases que heredan de esta llamadas **Weapon** y **Key**. 
>
>   Cada una de ellas contiene las propiedades que debe tener cada uno de los objetos y facilita mucho la creación de nuevos. De esta forma, si quisieramos añadir un arma o una llave, sencillamente crearíamos un nuevo scriptable object y le asignaríamos los valores correspondientes. 
>
>   <p>
>   <img style="align: center" width="200px"src="images/ak_47.png") />
>   <img style="align: center" width="200px"src="images/pistol.png") />
>   <img style="align: center" width="200px"src="images/key.png") />
>   </p>
>
>   Estos scriptable objects son justamente los mencionados en el apartado **PlayerScripts** cuando tratamos con items de tipo **Weapon** o tipo **Key**.
>
> - **Supplies:** De la misma forma que se ha hecho con las puertas y los enemigos, se ha usado la interfaz **ISupply** para definir un método común en todos los tipos de consumibles que nos permita recogerlos del suelo. Dicho método se llama **PickupSupply** y, además de heredar de la interfaz, también hereda de **MonoBehaviour** para hacer uso de los métodos **Update** y **Start**. 
>
>   La estratégia a seguir es exactamente la misma que al recoger armas del suelo. Si pulsamos la tecla Q, se lanza un rayo desde el centro de la pantalla hacia donde miramos y, si colisiona con un consumible, ejecuta el código correspondiente. Si es un consumible de vida, nos sumará salud; si es de escudo, nos sumará escudo; y si es de munición, sumará munición al arma equipada. 
>
> - **Drops:** Por último, pero no menos importante, tratamos los drops de los enemigos mediante el uso de ScriptableObjects y un struct de probabilidades. Para ello, se hace uso de la clase **LootTable**, que hereda de **ScriptableObject** y que contiene un struct con la rareza del objeto y el objeto en cuestión.
>
>   ````
>   [CreateAssetMenu(fileName = "LootData", menuName = "Loot Table")]
>   public class LootTable : ScriptableObject
>   { 
>       [System.Serializable]
>       public struct Probabilities
>       {
>           public int rarity;
>           public GameObject reward;
>       }
>   
>       public Probabilities[] probabilities; 
>       
>   }
>   ````
>
>   <p>
>   <img style="align: center" width="400px"src="images/loot_table.png") />
>   </p>
>
>   Como podemos observar, en este caso se ha escogido que los enemigos puedan soltar supplies y llaves. De esta misma manera, se podría haber añadido un prefab de cualquier tipo de arma para que también las puedan soltar.
>
>   Después, disponemos de un script **LootSystem** que se encarga de gestionar los drops. Para ello se hace uso de un comparador de rarezas, que ordena el array de probabilidades para asegurarnos que siempre va de mayor a menor. Por último, se usa el método **SpawnLoot()**, que recibe por parámetro el punto donde se va a soltar el objeto, un factor que ayuda a que los objetos raros salgan con más facilidad, la cantidad de items a soltar y la probabilidad de drop. 
>
>   Así pues, mediante nuestra **LootTable** y el array de probabilidades, generamos aleatoriamente la probabilidad de soltar un objeto y la comparamos con el parámetro de entrada del método para determinar si hay o no que soltarlo. En caso afirmativo, recorremos el array de probabilidades y comprobamos las rarezas para decidir cuál es el objeto seleccinado a instanciar. Una vez se ha soltado el objeto, salimos del loop para asegurarnos que sólo se suelta 1 objeto por enemigo. 
>
>   Para terminar, se hace uso del script **LootableObj** con la intención de añadirlo a los enemigos, cofres o cualquier elemento que pueda soltar objetos. Dicho script, llama al método **SpawnLoot()** de nuestro **LootSystem**.
>
>   <p>
>   <img style="align: center" width="200px"src="images/drop_1.png") />
>   <img style="align: center" width="200px"src="images/drop_2.png") />
>   <img style="align: center" width="200px"src="images/drop_3.png") />
>   </p>
</div>

