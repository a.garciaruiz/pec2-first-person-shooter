using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSupply : MonoBehaviour, ISupply
{
    private Camera cam;
    private GameObject player;
    private CharacterStats characterStats;
    private int shieldToAdd = 30;

    [SerializeField] LayerMask pickupLayer;

    private void Start()
    {
        GetReferences();
    }

    private void Update()
    {
        PickupSupply();
    }

    public void PickupSupply()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));

            if (Physics.Raycast(ray, out hit, 3, pickupLayer))
            {
                if (characterStats.shield == characterStats.maxShield)
                {
                    Debug.Log("Shield already full");
                }
                else
                {
                    characterStats.AddShield(shieldToAdd);
                    Destroy(hit.transform.gameObject);
                }
            }
        }
    }

    private void GetReferences()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        cam = player.GetComponentInChildren<Camera>();
        characterStats = player.GetComponent<CharacterStats>();
    }
}
