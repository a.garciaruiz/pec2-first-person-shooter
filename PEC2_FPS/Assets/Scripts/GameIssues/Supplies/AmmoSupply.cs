using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoSupply : MonoBehaviour, ISupply
{
    private GameObject player;
    private Camera cam;
    private Shooting shooting;
    private EquipmentManager manager;
    private Inventory inventory;
    private Weapon currentWeapon;

    private int ammoToAdd;
    private int storageToAdd;

    [SerializeField] private LayerMask pickupLayer;

    private void Start()
    {
        GetReferences();
    }

    private void Update()
    {
        currentWeapon = inventory.GetItem(manager.currentlyEquipedWeapon);
        PickupSupply();
    }

    public void PickupSupply()
    {
        if (currentWeapon.weaponStyle == WeaponStyle.Primary)
        {
            ammoToAdd = currentWeapon.magazineSize - shooting.primaryCurrentAmmo;
            storageToAdd = currentWeapon.storedAmmo - shooting.primaryCurrentAmmoStorage;
        }

        if(currentWeapon.weaponStyle == WeaponStyle.Secondary)
        {
            ammoToAdd = currentWeapon.magazineSize - shooting.secondaryCurrentAmmo;
            storageToAdd = currentWeapon.storedAmmo - shooting.secondaryCurrentAmmoStorage;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));

            if (Physics.Raycast(ray, out hit, 3, pickupLayer))
            {
                shooting.AddAmmo(manager.currentlyEquipedWeapon, ammoToAdd, storageToAdd);
                Destroy(hit.transform.gameObject);
            }
        }
    }

    private void GetReferences()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        cam = player.GetComponentInChildren<Camera>();
        shooting = player.GetComponent<Shooting>();
        manager = player.GetComponent<EquipmentManager>();
        inventory = player.GetComponent<Inventory>();
    }
}
