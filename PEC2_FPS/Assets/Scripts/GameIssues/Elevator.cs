using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    private bool shouldMove = false;
    private bool onOrigin = true;

    [SerializeField]
    private Transform origin, goal;

    [SerializeField]
    private GameObject p0Door, p1Door;

    private float speed = 1;

    private void FixedUpdate()
    {
        if (shouldMove)
        {
            if (onOrigin)
            {
                if(transform.position == goal.position)
                {
                    shouldMove = false;
                    onOrigin = false;
                }
                else
                    MoveTo(goal.position);
            }
            else
            {
                if(transform.position == origin.position)
                {
                    shouldMove = false;
                    onOrigin = true;
                }
                else
                    MoveTo(origin.position);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (!shouldMove)
                shouldMove = true;

            other.transform.parent = this.transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            other.transform.parent = null;
    }

    private void MoveTo(Vector3 pos)
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, pos, step);
    }
}
