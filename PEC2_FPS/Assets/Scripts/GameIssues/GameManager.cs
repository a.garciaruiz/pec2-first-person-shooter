using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject player;
    public Transform checkPoint;
    [SerializeField] private PlayerStats playerStats;
    public static int enemiesLeft;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        enemiesLeft = enemies.Length;
    }

    void Update()
    {
        if (enemiesLeft == 0)
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        ListenerMethods.ChangeScene(Scenes.gameOver);
    }

    public void Respawn()
    {
        player.GetComponent<CharacterController>().enabled = false;
        player.transform.position = checkPoint.position;
        player.transform.rotation = checkPoint.rotation;
        player.GetComponent<CharacterController>().enabled = true;
        ResetStats();
    }

    private void ResetStats()
    {
        playerStats.isDead = false;
        playerStats.SetHealthTo(playerStats.maxHealth);
        playerStats.SetShieldTo(playerStats.maxShield);
    }
}
