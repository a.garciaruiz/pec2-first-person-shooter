using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseManager : MonoBehaviour
{
    private Scene scene;
    [SerializeField] private GameObject pausePanel;

    [SerializeField] private Button resumeButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button quitButton;

    private List<Button> menuButtons = new List<Button>();

    void Start()
    {
        scene = SceneManager.GetActiveScene();

        // Añadimos los botones a la lista para eliminar los listeners
        menuButtons.Add(resumeButton);
        menuButtons.Add(restartButton);
        menuButtons.Add(quitButton);

        resumeButton.onClick.AddListener(() => ListenerMethods.ResumeGame(pausePanel));
        restartButton.onClick.AddListener(() => ListenerMethods.ChangeScene(scene.name));
        quitButton.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.mainMenu));

        // Desactivamos el panel de pausa
        pausePanel.SetActive(false);
    }

    void Update()
    {
        if (Cursor.visible == false)
        {
            // Si apretamos ESC
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Desactivamos los controles del FPS Controller
                DisableControls();
                // Activamos el panel de pausa
                pausePanel.SetActive(true);
                // Paramos el juego
                Time.timeScale = 0;
            }
        }
    }

    private void DisableControls()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        player.GetComponent<FirstPersonController>().enabled = false;
        player.GetComponent<Shooting>().enabled = false;
    }

    private void OnDestroy()
    {
        // Eliminamos los listeners
        ListenerMethods.RemoveListeners(menuButtons);
    }
}
