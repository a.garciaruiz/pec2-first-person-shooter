using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class ListenerMethods
{
    // Restaura los valores si se ha parado el juego
    public static void ResumeGame(GameObject obj)
    {
        obj.SetActive(false);
        RestoreTime();
        EnableControls();
    }

    // Cambia de escena
    public static void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
        RestoreTime();
        EnableControls();
    }

    // Cierra la app
    public static void QuitApp()
    {
        Debug.Log("APPLICATION CLOSED!");
        Application.Quit();
    }

    // Elimina los listeners
    public static void RemoveListeners(List<Button> buttons)
    {
        if (buttons != null)
        {
            foreach (Button button in buttons)
            {
                button.onClick.RemoveAllListeners();
            }
        }
    }

    // Restablece el tiempo de juego
    private static void RestoreTime()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    private static void EnableControls()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Cursor.lockState = CursorLockMode.None;
        if (player != null)
        {
            player.GetComponent<FirstPersonController>().enabled = true;
            player.GetComponent<Shooting>().enabled = true;
        }

    }
}