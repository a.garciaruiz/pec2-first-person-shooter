using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorManager : MonoBehaviour
{
    private TriggerDoor triggerDoor;
    private KeyDoor keyDoor;
    [HideInInspector] public Inventory inventory;
    [HideInInspector] public AudioSource audioSource;
    public GameObject keyPanel;

    private IDoor currentDoor;

    private void Start()
    {
        GetReferences();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.CompareTag("TriggerDoor"))
        {
            currentDoor = triggerDoor;
        }
        else if (gameObject.CompareTag("KeyDoor"))
        {
            currentDoor = keyDoor;
        }

        if (other.gameObject.CompareTag("Player"))
        {
            GameObject[] decals = GameObject.FindGameObjectsWithTag("Decal");
            foreach(GameObject decal in decals)
            {
                Destroy(decal);
            }
            currentDoor.OpenDoor();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            currentDoor.CloseDoor();
        }
    }

    private void GetReferences()
    {
        triggerDoor = new TriggerDoor(this);
        keyDoor = new KeyDoor(this);
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        audioSource = GetComponent<AudioSource>();
    }
}
