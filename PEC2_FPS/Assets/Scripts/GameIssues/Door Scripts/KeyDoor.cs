using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDoor : IDoor
{
    private DoorManager doorManager;

    public KeyDoor(DoorManager door)
    {
        doorManager = door;
    }

    public void OpenDoor()
    {
        if (doorManager.inventory.hasKey)
        {
            doorManager.audioSource.Play();
            doorManager.GetComponent<Animator>().Play("Door_Open");
        }
        else
        {
            doorManager.keyPanel.SetActive(true);
        }
    }

    public void CloseDoor()
    {
        doorManager.keyPanel.SetActive(false);
    }
}
