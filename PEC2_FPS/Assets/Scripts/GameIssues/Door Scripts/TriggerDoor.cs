using UnityEngine;

public class TriggerDoor : IDoor
{
    private DoorManager doorManager;

    public TriggerDoor(DoorManager door)
    {
        doorManager = door;
    }

    public void OpenDoor()
    {
        doorManager.audioSource.Play();
        doorManager.GetComponent<Animator>().Play("Door_Open");
    }

    public void CloseDoor()
    {
        doorManager.GetComponent<Animator>().Play("Door_Close");
    }
}
