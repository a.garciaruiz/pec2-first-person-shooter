using UnityEngine;

public class PatrolState : IEnemyState
{
    EnemyAI enemyAI;
    private int nextWayPoint = 0;

    public PatrolState(EnemyAI enemy)
    {
        enemyAI = enemy;
    }

    public void UpdateState()
    {
        enemyAI.spotlight.color = Color.green;
        enemyAI.navMeshAgent.destination = enemyAI.wayPoints[nextWayPoint].position;

        if (!enemyAI.navMeshAgent.pathPending && enemyAI.navMeshAgent.remainingDistance < 0.5f)
        {
            nextWayPoint = (nextWayPoint + 1) % enemyAI.wayPoints.Length;
        }
    }

    public void Impact()
    {
        ToAttackState();
    }

    public void ToAlertState()
    {
        Debug.Log("Alert State");
        enemyAI.navMeshAgent.isStopped = true;    
        enemyAI.currentState = enemyAI.alertState;
    }


    public void ToAttackState()
    {
        enemyAI.navMeshAgent.isStopped = true;
        enemyAI.shouldFollow = true;
        enemyAI.currentState = enemyAI.attackState;
    }

    public void ToPatrolState() {}

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            ToAlertState();
        }
    }

    public void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            ToAlertState();
        }
    }

    public void OnTriggerExit(Collider col) {}
}
