using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    public int currentlyEquipedWeapon = 0;
    private GameObject currentWeapon = null;
    public Transform currentWeaponBarrel = null;

    [SerializeField] private Transform weaponHolder = null;
    private Inventory inventory;

    [SerializeField] Weapon defaultWeapon = null;
    [SerializeField] private PlayerHUD playerHUD;
    [SerializeField] private Shooting shooting;

    private void Start()
    {
        GetReferences();
        InitVariables();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1) && currentlyEquipedWeapon != 0)
        {
            UnequipWeapon();
            EquipWeapon(inventory.GetItem(0));
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && currentlyEquipedWeapon != 1)
        {
            if (inventory.GetItem(1) != null)
            {
                UnequipWeapon();
                EquipWeapon(inventory.GetItem(1));
            }
        }
    }

    private void EquipWeapon(Weapon weapon)
    {
        playerHUD.UpdateWeaponUI(weapon);
        if((int)weapon.weaponStyle == 0)
        {
            playerHUD.UpdateAmmoUI(shooting.primaryCurrentAmmo, shooting.primaryCurrentAmmoStorage);
        }
        if((int)weapon.weaponStyle == 1)
        {
            playerHUD.UpdateAmmoUI(shooting.secondaryCurrentAmmo, shooting.secondaryCurrentAmmoStorage);
        }
        currentlyEquipedWeapon = (int)weapon.weaponStyle;
        currentWeapon = Instantiate(weapon.prefab, weaponHolder);
        currentWeaponBarrel = currentWeapon.transform.GetChild(0);
    }

    private void UnequipWeapon()
    {
        Destroy(currentWeapon);
    }

    private void InitVariables()
    {
        inventory.AddItem(defaultWeapon);
        EquipWeapon(inventory.GetItem(0));
    }

    private void GetReferences()
    {
        inventory = GetComponent<Inventory>();
        shooting = GetComponent<Shooting>();
    }
}
