using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    private Camera cam;
    private Inventory inventory;
    private EquipmentManager equipmentManager;
    private PlayerHUD playerHUD;
    private AudioSource gunAudio;
    [SerializeField] AudioClip reloadClip;

    private float lastFireTime;
    private bool canShoot;
    private bool canReload;

    [HideInInspector] public int primaryCurrentAmmo;
    [HideInInspector] public int primaryCurrentAmmoStorage;
    private bool primaryMagazineIsEmpty = false;

    [HideInInspector] public int secondaryCurrentAmmo;
    [HideInInspector] public int secondaryCurrentAmmoStorage;
    private bool secondaryMagazineIsEmpty = false;

    
    [SerializeField] private GameObject decalPrefab;
    private GameObject decalInstance;
    public static List<GameObject> decals = new List<GameObject>();
    private int maxDecalsAllowed = 10;

    private void Start()
    {
        GetReferences();
        canShoot = true;
        canReload = true;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Shoot();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload(equipmentManager.currentlyEquipedWeapon);
            gunAudio.clip = reloadClip;
            gunAudio.Play();
        }
    }

    private void RaycastShoot(Weapon currentWeapon)
    {
        Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
        RaycastHit hit;

        float currentWeaponRange = currentWeapon.range;

        if (Physics.Raycast(ray, out hit, currentWeaponRange))
        {
            if (hit.collider.gameObject.tag == "Enemy")
            {
                CharacterStats enemyStats = hit.collider.gameObject.GetComponentInParent<CharacterStats>();
                EnemyAI enemyAI = hit.collider.gameObject.GetComponentInParent<EnemyAI>();
                enemyAI.isHit = true;
                DealDamage(enemyStats, currentWeapon.damage);
            }

            if (hit.collider.gameObject.tag != "Enemy")
            {
                decalInstance = GameObject.Instantiate(decalPrefab, hit.point + hit.normal * 0.01f, Quaternion.FromToRotation(Vector3.forward, -hit.normal));
                decalInstance.transform.SetParent(hit.transform);
            }

            if (decals.Count >= maxDecalsAllowed)
            {
                GameObject.Destroy(decals[0]);
                decals.RemoveAt(0);
            }

            decals.Add(decalInstance);
        }

        Instantiate(currentWeapon.muzzleFlashParticles, equipmentManager.currentWeaponBarrel);
    }

    private void Shoot()
    {
        Weapon currentWeapon = inventory.GetItem(equipmentManager.currentlyEquipedWeapon);

        CheckCanShoot(equipmentManager.currentlyEquipedWeapon);
        InitAudio(currentWeapon);

        if (canShoot && canReload)
        {
            if (Time.time > lastFireTime + currentWeapon.fireRate)
            {
                lastFireTime = Time.time;

                RaycastShoot(currentWeapon);
                UseAmmo((int)currentWeapon.weaponStyle, 1, 0);
                gunAudio.clip = currentWeapon.fireFX;
                gunAudio.Play();
            }
        }
        else
            Debug.Log("Empty Magazine");
    }

    private void UseAmmo(int slot, int currentAmmoUsed, int currentStoredAmmoUsed)
    {
        if(slot == 0)
        {
            if(primaryCurrentAmmo <= 0)
            {
                primaryMagazineIsEmpty = true;
                CheckCanShoot(equipmentManager.currentlyEquipedWeapon);
            }
            else
            {
                primaryCurrentAmmo -= currentAmmoUsed;
                primaryCurrentAmmoStorage -= currentStoredAmmoUsed;
                playerHUD.UpdateAmmoUI(primaryCurrentAmmo, primaryCurrentAmmoStorage);
            }
        }

        if(slot == 1)
        {
            if(secondaryCurrentAmmo <= 0)
            {
                secondaryMagazineIsEmpty = true;
                CheckCanShoot(equipmentManager.currentlyEquipedWeapon);
            }
            else
            {
                secondaryCurrentAmmo -= currentAmmoUsed;
                secondaryCurrentAmmoStorage -= currentStoredAmmoUsed;
                playerHUD.UpdateAmmoUI(secondaryCurrentAmmo, secondaryCurrentAmmoStorage);
            }
        }
    }

    public void AddAmmo(int slot, int currentAmmoAdded, int currentStoredAmmoAdded)
    {
        if (slot == 0)
        {
            primaryCurrentAmmo += currentAmmoAdded;
            primaryCurrentAmmoStorage += currentStoredAmmoAdded;
            playerHUD.UpdateAmmoUI(primaryCurrentAmmo, primaryCurrentAmmoStorage);
        }

        if (slot == 1)
        {
            secondaryCurrentAmmo += currentAmmoAdded;
            secondaryCurrentAmmoStorage += currentStoredAmmoAdded;
            playerHUD.UpdateAmmoUI(secondaryCurrentAmmo, secondaryCurrentAmmoStorage);
        }
    }

    private void Reload(int slot)
    {
        if (canReload)
        {
            if (slot == 0)
            {
                int ammoToReload = inventory.GetItem(slot).magazineSize - primaryCurrentAmmo;

                if (primaryCurrentAmmoStorage >= ammoToReload)
                {
                    if (primaryCurrentAmmo == inventory.GetItem(slot).magazineSize)
                    {
                        canReload = false;
                        Debug.Log("Magazine full");
                    }
                    else
                        canReload = true;

                    AddAmmo(slot, ammoToReload, 0);
                    UseAmmo(slot, 0, ammoToReload);

                    primaryMagazineIsEmpty = false;
                    CheckCanShoot(slot);
                }
                else
                    Debug.Log("Not enough ammo to reaload");
            }

            if (slot == 1)
            {
                int ammoToReload = inventory.GetItem(slot).magazineSize - secondaryCurrentAmmo;

                if (secondaryCurrentAmmoStorage >= ammoToReload)
                {
                    if (secondaryCurrentAmmo == inventory.GetItem(slot).magazineSize)
                    {
                        canReload = false;
                        Debug.Log("Magazine full");
                    }
                    else
                        canReload = true;

                    AddAmmo(slot, ammoToReload, 0);
                    UseAmmo(slot, 0, ammoToReload);

                    secondaryMagazineIsEmpty = false;
                    CheckCanShoot(slot);
                }
                else
                    Debug.Log("Not enough ammo to reaload");
            }
        }
        else
            Debug.Log("Can't reload now");
    }

    public void DealDamage(CharacterStats statsToDamage, int damage)
    {
        statsToDamage.TakeDamage(damage);
    }

    private void CheckCanShoot(int slot)
    {
        if(slot == 0)
        {
            if (primaryMagazineIsEmpty)
                canShoot = false;
            else
                canShoot = true;
        }

        if(slot == 1)
        {
            if (secondaryMagazineIsEmpty)
                canShoot = false;
            else
                canShoot = true;
        }
    }

    public void InitAmmo(int slot, Weapon weapon)
    {
        if(slot == 0)
        {
            primaryCurrentAmmo = weapon.magazineSize;
            primaryCurrentAmmoStorage = weapon.storedAmmo;
        }

        if(slot == 1)
        {
            secondaryCurrentAmmo = weapon.magazineSize;
            secondaryCurrentAmmoStorage = weapon.storedAmmo;
        }
    }

    private void InitAudio(Weapon weapon)
    {
        gunAudio.clip = weapon.fireFX;
    }

    private void GetReferences()
    {
        cam = GetComponentInChildren<Camera>();
        inventory = GetComponent<Inventory>();
        equipmentManager = GetComponent<EquipmentManager>();
        playerHUD = GetComponent<PlayerHUD>();
        gunAudio = GetComponent<AudioSource>();
    }
}
