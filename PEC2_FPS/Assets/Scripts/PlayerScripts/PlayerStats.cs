using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    [SerializeField] private PlayerHUD playerHUD;
    [SerializeField] private AudioClip hurtClip;


    private void Start()
    {
        InitVariables();
        GetReferences();
    }

    private void GetReferences()
    {
        playerHUD = GetComponent<PlayerHUD>();
    }

    public override void Die()
    {
        base.Die();
        GameManager gameManager = GameObject.FindObjectOfType<GameManager>();
        gameManager.Respawn();
    }

    public override void CheckHealth()
    {
        base.CheckHealth();
        playerHUD.UpdateHealth(health, maxHealth);
    }

    public override void CheckShield()
    {
        base.CheckShield();
        playerHUD.UpdateShield(shield, maxShield);
    }

    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);
        PlayHurtSound();
    }

    private void PlayHurtSound()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = hurtClip;
        audioSource.Play();
    }
}
