using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPickup : MonoBehaviour
{
    [SerializeField] private float pickupRange;
    [SerializeField] private LayerMask pickupLayer;
    [SerializeField] private LayerMask keyLayer;
    [SerializeField] private GameObject pickupPanel;
    [SerializeField] private Text pickupPanelText;
    [SerializeField] private AudioClip pickupClip;

    private Inventory inventory;
    private Camera cam;

    private void Start()
    {
        GetReferences();
        InitVariables();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AudioSource audioSource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
            audioSource.clip = pickupClip;
            audioSource.Play();

            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));


            if (Physics.Raycast(ray, out hit, pickupRange, pickupLayer))
            {
                Weapon newWeapon = hit.transform.GetComponent<ItemObj>().item as Weapon;
                inventory.AddItem(newWeapon);
                Destroy(hit.transform.gameObject);
                pickupPanel.SetActive(true);
                pickupPanelText.text = "WEAPON ADDED TO YOUR INVENTORY";
                StartCoroutine(HidePanel());
            }

            if (Physics.Raycast(ray, out hit, pickupRange, keyLayer))
            {
                Key newItem = hit.transform.GetComponent<ItemObj>().item as Key;
                inventory.AddItem(newItem);
                Destroy(hit.transform.gameObject);
                pickupPanel.SetActive(true);
                pickupPanelText.text = "KEY ADDED TO YOUR INVENTORY";
                StartCoroutine(HidePanel());
            }
        }
    }

    private void InitVariables()
    {
        pickupPanel.SetActive(false);
    }

    private void GetReferences()
    {
        cam = GetComponentInChildren<Camera>();
        inventory = GetComponent<Inventory>();
    }

    IEnumerator HidePanel()
    {
        yield return new WaitForSeconds(1);
        pickupPanel.SetActive(false);
    }
}
